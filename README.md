# README #

* **Purpose:** This is a simple utility plugin with helpful date methods build on the Joda-Time API.
* **Version:** 1.0.1

## Installation ##
In `BuildConfig.groovy` add the following plugin dependency:

    plugins {
        ...
        compile ":date-util:1.0.1"
        ...
    }

## Sample Usage ##
The [DateUtil](https://bitbucket.org/sharptop/date-util/src/58c8898730a9d5ff8bfae9a8c887bbfa087b6b32/grails-app/utils/com/sharptop/dateUtil/DateUtil.groovy?at=master) class contains the bulk of the logic.

**Start of Today:** Find Midnight This Morning / Last Night

```
#!groovy
    import static com.sharptop.dateUtil.DateUtil.*

    static findTodays() {
        def c = createCriteria()
        def results = c.list {
            and {
                ge ("receivedAt", DateUtil.startOfToday())
            }
        }
    }
```

**Earlier:** Create a date a little earlier than right now.

```
#!groovy
    import static com.sharptop.dateUtil.DateUtil.*
    import static javax.management.timer.Timer.*

    Date receivedAt = earlier(Timer.ONE_MINUTE))
```


**Are Similar:** Compare two dates within a specified degree of tolerance.  *Very useful in testing time related units of work.*

```
#!groovy
    def "test later" (long howLong) {
        when:
        Date result = later(howLong)

        then:
        areSimilar(result, new Date(now().time + howLong))

        where:
        howLong << [ONE_DAY, ONE_HOUR, ONE_MINUTE]
    }
```

see [DateUtil](https://bitbucket.org/sharptop/date-util/src/58c8898730a9d5ff8bfae9a8c887bbfa087b6b32/grails-app/utils/com/sharptop/dateUtil/DateUtil.groovy?at=master) for the full API.

## Contribution guidelines ##

If you would like to contribute, that would be greatly appreciated.  

* **Main Repo:** [https://bitbucket.org/sharptop/date-util](https://bitbucket.org/sharptop/date-util)
* **Project Owner:** [SharpTopTony](https://bitbucket.org/SharpTopTony)