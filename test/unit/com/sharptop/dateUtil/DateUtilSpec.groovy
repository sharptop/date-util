
package com.sharptop.dateUtil

import static com.sharptop.dateUtil.DateUtil.*
import static javax.management.timer.Timer.*
import grails.test.mixin.*
import grails.test.mixin.support.GrailsUnitTestMixin

import org.joda.time.DateTime
import org.joda.time.LocalDate

import spock.lang.Specification
import spock.lang.Unroll

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
class DateUtilSpec extends Specification {

    DateUtil dateUtil

    def setup() {
        dateUtil = new DateUtil()
    }

    def "test now" () {
        expect:
        areSimilar(new Date(), now())
    }

    def "test earlier" (long howLong) {
        when:
        Date result = earlier(howLong)

        then:
        areSimilar(result, new Date(now().time - howLong))

        where:
        howLong << [ONE_DAY, ONE_HOUR, ONE_MINUTE]
    }

    def "test later" (long howLong) {
        when:
        Date result = later(howLong)

        then:
        areSimilar(result, new Date(now().time + howLong))

        where:
        howLong << [ONE_DAY, ONE_HOUR, ONE_MINUTE]
    }

    @Unroll
    def "test #foo" () {
        expect:
        println foo

        where:
        foo << ["tony", "erskine"]
    }

    def "test areSimilar" (Date date1, Date date2, boolean expected) {
        expect:
        areSimilar(date1, date2) == expected

        where:
        date1           | date2         | expected
        new Date()      | new Date()    | true
        new Date(100)   | new Date(109) | true
        new Date(100)   | new Date(110) | false
    }

    def "test areSimilar with max difference" (Date date1, Date date2, long maxDiff, boolean expected) {
        expect:
        areSimilar(date1, date2, maxDiff) == expected

        where:
        date1           | date2                 | maxDiff       | expected
        new Date()      | new Date()            | 5             | true
        new Date(100)   | new Date(109)         | 5             | false
        new Date(1)     | new Date(ONE_MINUTE)  | ONE_MINUTE    | true
    }

    def "test newDate" () {
        when:
        int year = 2014
        int month = 10
        int day = 4

        then:
        newDate(year, month, day) == new LocalDate(year, month, day).toDate();
    }

    def "test start of day with java date" () {
        when:
        Date dateNoTime = new Date(1412568000000) // Oct 6, 2014 00:00
        Date dateWithTime = new Date(1412628243127) // Oct 6, 2014 16:45-ish

        then:
        dateNoTime != dateWithTime
        dateNoTime == startOfDay(dateWithTime)
    }

    def "test start of day with joda date time" () {
        when:
        DateTime dateNoTime = new DateTime(1412568000000) // Oct 6, 2014 00:00
        DateTime dateWithTime = new DateTime(1412628243127) // Oct 6, 2014 16:45-ish

        then:
        dateNoTime != dateWithTime
        dateNoTime == startOfDay(dateWithTime)
    }

    def "test start of today" () {
        expect:
        startOfToday() == startOfDay(now())
    }
}
