package com.sharptop.dateUtil
import org.joda.time.DateTime
import org.joda.time.LocalDate

class DateUtil {

    static now() {
        new Date()
    }

    static earlier(long howLong) {
        new Date(now().time - howLong)
    }

    static later(long howLong) {
        new Date(now().time + howLong)
    }

    static areSimilar(Date date1, Date date2, long maxDiff = 10) {
        Math.abs(date1.time - date2.time) < maxDiff
    }

    /**
     * Creates a new java.util.Date with the specified year, month, day
     *
     * @see java.util.Date
     * @param year
     * @param month
     * @param day
     * @return a new date object
     */
    static Date newDate(int year, int month, int day) {
        new LocalDate(year, month, day).toDate()
    }

    /**
     * Removes all time information (hours, minutes, seconds, millis) off of a date.
     *
     * @param date
     * @return
     */
    static Date startOfDay(Date date) {
        date ? (new LocalDate(date.getTime())).toDate() : null
    }

    /**
     * Removes all time information (hours, minutes, seconds, millis) off of a date.
     *
     * @param date
     * @return
     */
    static DateTime startOfDay(DateTime date) {
        date ? date.toLocalDate().toDateTimeAtStartOfDay() : null
    }

    static startOfToday() {
        startOfDay(now())
    }
}
