class DateUtilGrailsPlugin {
    // the plugin version
    def version = "1.0.1"
    // the version or versions of Grails the plugin is designed for
    def grailsVersion = "2.4 > *"
    // resources that are excluded from plugin packaging
    def pluginExcludes = [
        "grails-app/views/error.gsp"
    ]

    def title = "Date Util Plugin" // Headline display name of the plugin
    def author = "Tony Erskine"
    def authorEmail = "tony@SharpTopSoftware.com"
    def description = '''\
This is a simple utility plugin with helpful date methods build on the Joda-Time API.
'''

    // URL to the plugin's documentation
    def documentation = "http://grails.org/plugin/date-util"

    // Extra (optional) plugin metadata

    // License: one of 'APACHE', 'GPL2', 'GPL3'
    def license = "APACHE"

    // Details of company behind the plugin (if there is one)
    def organization = [ name: "SharpTop Software", url: "http://www.sharptopsoftware.com/" ]

    // Any additional developers beyond the author specified above.
//    def developers = [ [ name: "Joe Bloggs", email: "joe@bloggs.net" ]]

    // Location of the plugin's issue tracker.
    def issueManagement = [ system: "JIRA", url: "https://sharptop.atlassian.net/browse/DU/" ]

    // Online location of the plugin's browseable source code.
    def scm = [ url: "https://bitbucket.org/sharptop/date-util" ]

    def doWithWebDescriptor = { xml ->
    }

    def doWithSpring = {
    }

    def doWithDynamicMethods = { ctx ->
    }

    def doWithApplicationContext = { ctx ->
    }

    def onChange = { event ->
    }

    def onConfigChange = { event ->
    }

    def onShutdown = { event ->
    }
}
